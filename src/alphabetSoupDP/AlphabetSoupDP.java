package alphabetSoupDP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class AlphabetSoupDP {

	public static void main(String[] args) {
		
		try(Scanner scanner = new Scanner(System.in);){
			boolean readAnotherInputFile = true;
			
			do {
				try {
					System.out.print("Enter file path for input file: ");
					String     filePath 	= scanner.next();
					FileReader fileReader 	= new FileReader(new File(filePath));

					readAndProcessInputFile(fileReader);
				} catch (FileNotFoundException fileNotFoundException) {
					System.out.println("Invalid file path.  Please enter a valid file path.");
				} catch (Exception exception) {
					throw new Exception(exception);
				}

				System.out.print("Would you like to read another input file? 'Y' or 'N': ");
				readAnotherInputFile = scanner.next().equalsIgnoreCase("Y") ? true : false;
			}while (readAnotherInputFile);
			
			System.out.println("See you next time!  Good bye.");
		}
		catch(Exception exception) {
			System.out.println("Error: " + exception.getMessage());
		}
	}
	
	private static void readAndProcessInputFile(FileReader 	fileReader) throws Exception {
		try(BufferedReader bufferedReader = new BufferedReader(fileReader)){
			// Read first line of input file to get the number of rows and columns in the word search puzzle.
			String inputLine 		= bufferedReader.readLine();
			int    numberOfRows 	= Integer.parseInt(inputLine.substring(0, inputLine.indexOf("x")));
			int    numberOfColumns	= Integer.parseInt(inputLine.substring(inputLine.indexOf("x") + 1));
			
			char[][] characterGrid 	= loadCharacterGrid(bufferedReader, numberOfRows, numberOfColumns);
			
			searchForWordsInGrid(bufferedReader, characterGrid);
		}
		catch(Exception exception) {
			throw new Exception(exception);
		}
	}

	private static char[][] loadCharacterGrid(BufferedReader bufferedReader, int numberOfRows, int numberOfColumns) throws Exception{
		char[][] characterGrid = new char [numberOfRows][numberOfColumns];
		String   inputLine;
		
		for(int i = 0; i < numberOfRows; i++) {
			inputLine = bufferedReader.readLine();
			
			for(int j = 0; j < numberOfColumns; j++) {
				// Instructions say to assume input file is correctly formatted.
				// Sample input files have spaces between each character in grid of characters.
				// As such, using (j*2) will read only even columns with data and skip the columns with spaces.
				characterGrid[i][j] = inputLine.charAt(j*2);
			}
		}

		return characterGrid;
	}
	
	private static void searchForWordsInGrid(BufferedReader bufferedReader, char[][] characterGrid) throws Exception{
		String wordToSearchFor = bufferedReader.readLine();
		
		// Read each word to be found and find it
		// Assume at least one word to search for
		do {
			// Words that have spaces in them will not include spaces when hidden in the grid of characters.
			wordToSearchFor = wordToSearchFor.replace(" ", "");
			
			searchForThisWordInGrid(characterGrid, wordToSearchFor);
			
			wordToSearchFor = bufferedReader.readLine();
		}while(wordToSearchFor != null);
	}
	
	private static void searchForThisWordInGrid(char[][] characterGrid, String wordToSearchFor) {
		int  lastRowOfGrid			= getLastRowOfGrid(characterGrid);
		int  lastColumnOfGrid 		= getLastColumnOfGrid(characterGrid);
		char firstLetterThisWord 	= wordToSearchFor.charAt(0);

		// ASSUMPTION: We are assuming that each word may appear more than once so we don't stop searching
		//             after finding a word.  This should not occur for a Sunday word search puzzle but
		//             we want to identify and report it if it does occur.
		
		// If we find a match on the first letter of the word we're searching for then we proceed to
		// validate the entire word in all directions: Up, Down, Forwards, Backwards and Diagonal
		for(int i = 0; i <= lastRowOfGrid; i++) {
			for(int j = 0; j <= lastColumnOfGrid; j++) {
				if(characterGrid[i][j] == firstLetterThisWord) {
					searchForward				(characterGrid, wordToSearchFor, i, j);
					searchBackward				(characterGrid, wordToSearchFor, i, j);
					searchUp					(characterGrid, wordToSearchFor, i, j);
					searchDown					(characterGrid, wordToSearchFor, i, j);

					searchDiagonalForwardUp		(characterGrid, wordToSearchFor, i, j);
					searchDiagonalForwardDown	(characterGrid, wordToSearchFor, i, j);
					searchDiagonalBackwardUp	(characterGrid, wordToSearchFor, i, j);
					searchDiagonalBackwardDown	(characterGrid, wordToSearchFor, i, j);
				}
			}
		}
	}
	
	private static void searchForward(char[][] characterGrid, String wordToSearchFor, int rowOfWord, int columnBeginningOfWord) {
		int lastColumnOfGrid	= getLastColumnOfGrid(characterGrid);
		int columnEndOfWord		= columnBeginningOfWord + incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the last column of the array then compare
		if(columnEndOfWord <= lastColumnOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			for(int j = columnBeginningOfWord; j <= columnEndOfWord; j++) {
				stringToCompare.append(characterGrid[rowOfWord][j]);
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowOfWord, columnBeginningOfWord, rowOfWord, columnEndOfWord);
		}
	}
	
	private static void searchBackward(char[][] characterGrid, String wordToSearchFor, int rowOfWord, int columnBeginningOfWord) {
		int firstColumnOfGrid	= 0;
		int columnEndOfWord		= columnBeginningOfWord - incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the first column of the array then compare
		if(columnEndOfWord >= firstColumnOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			for(int j = columnBeginningOfWord; j >= columnEndOfWord; j--) {
				stringToCompare.append(characterGrid[rowOfWord][j]);
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowOfWord, columnBeginningOfWord, rowOfWord, columnEndOfWord);
		}
	}

	private static void searchUp(char[][] characterGrid, String wordToSearchFor, int rowBeginningOfWord, int column) {
		int firstRowOfGrid	= 0;
		int rowEndOfWord	= rowBeginningOfWord - incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the first row of the array then compare
		if(rowEndOfWord >= firstRowOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			for(int i = rowBeginningOfWord; i >= rowEndOfWord; i--) {
				stringToCompare.append(characterGrid[i][column]);
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowBeginningOfWord, column, rowEndOfWord, column);
		}
	}

	private static void searchDown(char[][] characterGrid, String wordToSearchFor, int rowBeginningOfWord, int column) {
		int lastRowOfGrid	= getLastRowOfGrid(characterGrid);
		int rowEndOfWord	= rowBeginningOfWord + incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the last row of the array then compare
		if(rowEndOfWord <= lastRowOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			for(int i = rowBeginningOfWord; i <= rowEndOfWord; i++) {
				stringToCompare.append(characterGrid[i][column]);
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowBeginningOfWord, column, rowEndOfWord, column);
		}
	}
	
	private static void searchDiagonalForwardUp(char[][] characterGrid, String wordToSearchFor, int rowBeginningOfWord, int columnBeginningOfWord) {
		int firstRowOfGrid		= 0;
		int rowEndOfWord		= rowBeginningOfWord - incrementToEndOfWord(wordToSearchFor);
		int lastColumnOfGrid	= getLastColumnOfGrid(characterGrid);
		int columnEndOfWord		= columnBeginningOfWord + incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the first row or last column of the array then compare
		if(rowEndOfWord    >= firstRowOfGrid && 
		   columnEndOfWord <= lastColumnOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			
			int j = columnBeginningOfWord;
			for(int i = rowBeginningOfWord; i >= rowEndOfWord; i--) {
				stringToCompare.append(characterGrid[i][j]);
				j++;
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowBeginningOfWord, columnBeginningOfWord, rowEndOfWord, columnEndOfWord);
		}
	}
	
	private static void searchDiagonalForwardDown(char[][] characterGrid, String wordToSearchFor, int rowBeginningOfWord, int columnBeginningOfWord) {
		int lastRowOfGrid		= getLastRowOfGrid(characterGrid);
		int rowEndOfWord		= rowBeginningOfWord + incrementToEndOfWord(wordToSearchFor);
		int lastColumnOfGrid	= getLastColumnOfGrid(characterGrid);
		int columnEndOfWord		= columnBeginningOfWord + incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the last row or last column of the array then compare
		if(rowEndOfWord    <= lastRowOfGrid && 
		   columnEndOfWord <= lastColumnOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			
			int j = columnBeginningOfWord;
			for(int i = rowBeginningOfWord; i <= rowEndOfWord; i++) {
				stringToCompare.append(characterGrid[i][j]);
				j++;
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowBeginningOfWord, columnBeginningOfWord, rowEndOfWord, columnEndOfWord);
		}
	}
	
	private static void searchDiagonalBackwardUp(char[][] characterGrid, String wordToSearchFor, int rowBeginningOfWord, int columnBeginningOfWord) {
		int firstRowOfGrid		= 0;
		int rowEndOfWord		= rowBeginningOfWord - incrementToEndOfWord(wordToSearchFor);
		int firstColumnOfGrid	= 0;
		int columnEndOfWord		= columnBeginningOfWord - incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the first row or first column of the array then compare
		if(rowEndOfWord    >= firstRowOfGrid && 
		   columnEndOfWord >= firstColumnOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			
			int j = columnBeginningOfWord;
			for(int i = rowBeginningOfWord; i >= rowEndOfWord; i--) {
				stringToCompare.append(characterGrid[i][j]);
				j--;
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowBeginningOfWord, columnBeginningOfWord, rowEndOfWord, columnEndOfWord);
		}
	}
	
	private static void searchDiagonalBackwardDown(char[][] characterGrid, String wordToSearchFor, int rowBeginningOfWord, int columnBeginningOfWord) {
		int lastRowOfGrid		= getLastRowOfGrid(characterGrid);
		int rowEndOfWord		= rowBeginningOfWord + incrementToEndOfWord(wordToSearchFor);
		int firstColumnOfGrid	= 0;
		int columnEndOfWord		= columnBeginningOfWord - incrementToEndOfWord(wordToSearchFor);
		
		// If wordToSearchFor does not extend beyond the last row or first column of the array then compare
		if(rowEndOfWord    <= lastRowOfGrid && 
		   columnEndOfWord >= firstColumnOfGrid) {
			StringBuilder stringToCompare = new StringBuilder();
			
			int j = columnBeginningOfWord;
			for(int i = rowBeginningOfWord; i <= rowEndOfWord; i++) {
				stringToCompare.append(characterGrid[i][j]);
				j--;
			}
			
			compareWordToString(wordToSearchFor, stringToCompare.toString(), rowBeginningOfWord, columnBeginningOfWord, rowEndOfWord, columnEndOfWord);
		}
	}
	
	private static int getLastRowOfGrid(char[][] characterGrid) {
		return characterGrid.length - 1;
	}

	private static int getLastColumnOfGrid(char[][] characterGrid) {
		return characterGrid[0].length - 1;
	}

	private static int incrementToEndOfWord(String wordToSearchFor) {
		return wordToSearchFor.length() - 1;
	}
	
	private static void compareWordToString(String wordToSearchFor, String stringToCompare, int beginRow, int beginColumn, int endRow, int endColumn) {
		if(wordToSearchFor.equals(stringToCompare.toString())) {
			printOutput(wordToSearchFor, beginRow, beginColumn, endRow, endColumn);
		}
	}
	
	private static void printOutput(String wordToSearchFor, int beginRow, int beginColumn, int endRow, int endColumn) {
		System.out.println(wordToSearchFor 												+ " " +
						   String.valueOf(beginRow) + ":" + String.valueOf(beginColumn) + " " +
						   String.valueOf(endRow) 	+ ":" + String.valueOf(endColumn));
	}
}
